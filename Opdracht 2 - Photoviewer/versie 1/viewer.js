/**
 * User: Teun Rutten
 * jslint browser: false
 * jslint devel: true
 * jslint node: true
 */
/*global window, document, location*/
(function () {
    "use strict";
    var lastZoomed, photoviewer;
    photoviewer = {
        images: [
            {file: "0004681.jpg", author: "auteur1", date: "23-09-2013", alt: "1"},
            {file: "0004713.jpg", author: "auteur2", date: "23-09-2013", alt: "1"},
            {file: "0004720.jpg", author: "auteur4", date: "23-09-2013", alt: "1"},
            {file: "0004731.jpg", author: "auteur5", date: "23-09-2013", alt: "1"},
            {file: "0004750.jpg", author: "auteur6", date: "23-09-2013", alt: "1"},
            {file: "0004755.jpg", author: "auteur7", date: "23-09-2013", alt: "1"}
        ],

        Showimages: function () { //maakt de plaatjes
            document.addEventListener("keyup", photoviewer.toetsen, false);
            var i,  maakPlaatje, imgId;
			var random = Math.random()*20-10;
            for (i = 0; i < photoviewer.images.length; i = i + 1) {
                maakPlaatje = document.createElement("img");
                maakPlaatje.setAttribute("src", "secret/img/" + photoviewer.images[i].file);
                maakPlaatje.setAttribute("class", "kleineFoto");
                maakPlaatje.setAttribute("onmouseup", "photoviewer.verwijder(this.id, event)");
                maakPlaatje.setAttribute("onclick", "photoviewer.zoom(event)");
                maakPlaatje.setAttribute("alt", photoviewer.images[i].alt);
				maakPlaatje.setAttribute("style", "-webkit-transform:rotate("+random+"deg)");
                imgId = "img" + i;
                photoviewer.images[i].imgId = imgId;
                maakPlaatje.setAttribute("id", imgId);
                maakPlaatje.oncontextmenu = photoviewer.stopContextMenu();
                document.body.appendChild(maakPlaatje);
                document.body.id = "bodyID";
            }
            lastZoomed = document.getElementById("bodyID").firstChild.nextSibling; //zet het laatst ingezoomde plaatje op het eerste plaatje
        },

        imgDetails: function () { //maakt de details van het plaatje
            var date = document.createElement("p"), i, auteur;
            for (i = 0; i < photoviewer.images.length; i = i + 1) {
                auteur = document.createElement("p");
                date = document.createElement("p");
                auteur.setAttribute("class", "onzichtbaar"); //zet hem standaard onzichtbaar
                auteur.setAttribute("id", "auteur" + i);
                auteur.textContent = "Auteur: " + photoviewer.images[i].author;
                document.body.appendChild(auteur);
                date.setAttribute("class", "onzichtbaar"); //zet hem standaard onzichtbaar
                date.setAttribute("id", "date" + i);
                date.textContent = "Datum: " + photoviewer.images[i].date;
                document.body.appendChild(date);
            }
        },
        verwijder: function (idImg, event) { //verwijderd het plaatje uit de array
            if (document.getElementById(idImg).className === "kleineFoto" && event.button === 2) {
                photoviewer.raster();
                var file = document.getElementById(idImg).src, fileParts, found, finished, j, element;
                fileParts = file.split("/");
                file = fileParts[fileParts.length - 1];
                found = false;
                finished = false;
                j = 0;
                while (!found && !finished) {
                    found = file === photoviewer.images[j].file;
                    finished = j > photoviewer.images.length;
                    j = j + 1;
                }
                j = j - 1;
                photoviewer.images.splice(j, 1);
                element = document.getElementById(idImg);
                document.getElementsByTagName("body")[0].removeChild(element);
            }
        },

        navigatieLinks: function (id) {  //zorgt dat het plaatje naar links wordt ingezoomed. (het vorige plaatje dus)
            var thisImage = document.getElementById(id).id, previousImage, huidigAuteur, vorigeAuteur, huidigeDatum, vorigeDatum, laatstePlaatje;
            previousImage = document.getElementById(id).previousElementSibling.id;
            huidigAuteur = "auteur" + (thisImage.substring(3, 5));
            vorigeAuteur = "auteur" + (previousImage.substring(3, 5));
            huidigeDatum = "date" + (thisImage.substring(3, 5));
            vorigeDatum = "date" + (previousImage.substring(3, 5));
            if (previousImage === document.getElementById("bodyID").firstChild.nextSibling.id) {
                laatstePlaatje = photoviewer.images.length - 1;
                previousImage = "img" + laatstePlaatje;
                vorigeAuteur = "auteur" + laatstePlaatje;
                vorigeDatum = "date" + laatstePlaatje;
            }
            if (document.getElementById(thisImage).className === "groteFoto") { //zet het vorige plaatje op groot en het huidige plaatje onzichtbaar
                document.getElementById(previousImage).className = "groteFoto";
                document.getElementById(thisImage).className = "onzichtbaar";
                document.getElementById(vorigeAuteur).className = "tekst";
                document.getElementById(huidigAuteur).className = "onzichtbaar";
                document.getElementById(vorigeDatum).className = "tekst";
                document.getElementById(huidigeDatum).className = "onzichtbaar";
            }
        },

        navigatieRechts: function (id) { //zorgt dat het plaatje naar rechts wordt ingezoomed. (het volgende plaatje dus)
            var aantalPlaatjes = String(photoviewer.images.length - 1), thisImage, nextImage, huidigAuteur, volgendeAuteur, huidigeDatum, volgendeDatum, eerstePlaatje;
            thisImage = document.getElementById(id).id;
            nextImage = document.getElementById(id).nextElementSibling.id;
            huidigAuteur = "auteur" + (thisImage.substring(3, 5));
            volgendeAuteur = "auteur" + (nextImage.substring(3, 5));
            huidigeDatum = "date" + (thisImage.substring(3, 5));
            volgendeDatum = "date" + (nextImage.substring(3, 5));
            if (nextImage.substring(3, 5) === aantalPlaatjes) {
                eerstePlaatje = 0;
                nextImage = "img" + eerstePlaatje;
                volgendeAuteur = "auteur" + eerstePlaatje;
                volgendeDatum = "date" + eerstePlaatje;
            }
            if (document.getElementById(thisImage).className === "groteFoto") { //zet het volgende plaatje op groot en het huidige plaatje onzichtbaar
                document.getElementById(nextImage).className = "groteFoto";
                document.getElementById(thisImage).className = "onzichtbaar";
                document.getElementById(volgendeAuteur).className = "tekst";
                document.getElementById(huidigAuteur).className = "onzichtbaar";
                document.getElementById(volgendeDatum).className = "tekst";
                document.getElementById(huidigeDatum).className = "onzichtbaar";
            }
        },

        zoom: function (event) { //zorgt ervoor dat je kan inzoomen en uitzoomen en past de laatste waarde van de LastZoomed aan
            var thisImage = event.target.id, thisNonImg, thisIntImg, auteurId, datumId, i, imgId;
            lastZoomed = event.target;
            thisNonImg = (thisImage.substring(3, 5));
            thisIntImg = parseInt(thisNonImg, 10);
            auteurId = "auteur" + thisIntImg;
            datumId = "date" + thisIntImg;
            if (document.getElementById(thisImage).className === "kleineFoto" && event.button === 0) {
                document.getElementById(thisImage).className = "groteFoto";
                document.getElementById(auteurId).className = "tekst";
                document.getElementById(datumId).className = "tekst";
                for (i = 0; i < photoviewer.images.length; i = i + 1) {
                    imgId = photoviewer.images[i].imgId;
                   // console.log(i, imgId);
                    if (document.getElementById(imgId).className === "kleineFoto") { //zet alle overige plaatjes onzichtbaar
                        document.getElementById(imgId).className = "onzichtbaar";
                    }
                }
            } else {
                for (i = 0; i < photoviewer.images.length; i = i + 1) { //maakt alle plaatjes weer klein en zichtbaar
                    imgId = photoviewer.images[i].imgId;
                    if (event.button === 0) {
                        document.getElementById(imgId).className = "kleineFoto";
                        document.getElementById(auteurId).className = "onzichtbaar";
                        document.getElementById(datumId).className = "onzichtbaar";
                    }
                }
            }
        },

        stopContextMenu: function () { //disabled rechtklik als je op een plaatje klikt
            return function (event) {
                event.preventDefault();
            };
        },

        toetsen: function (event) { //zorgt ervoor dat als je de pijltjes indrukt dat dan het links of rechts navigeren wordt aangeroepen
            var i, imgId, auteurTekst, datumTekst;
            if (event.keyCode === 37) {
                photoviewer.navigatieLinks(document.getElementsByClassName("groteFoto")[0].id, event);
            }
            if (event.keyCode === 39) {
                photoviewer.navigatieRechts(document.getElementsByClassName("groteFoto")[0].id, event);
            }
            if (event.keyCode === 32) { //zorgt ervoor dat je met spatie kan in en uitzoomen
                auteurTekst = "auteur" + (lastZoomed.id.substring(3, 5));
                datumTekst = "date" + (lastZoomed.id.substring(3, 5));
                if (lastZoomed.className === "groteFoto") {
                    lastZoomed.className = "kleineFoto";
                    for (i = 0; i < photoviewer.images.length; i = i + 1) {
                        imgId = photoviewer.images[i].imgId;
                        document.getElementById(imgId).className = "kleineFoto";
                        document.getElementById(auteurTekst).className = "onzichtbaar";
                        document.getElementById(datumTekst).className = "onzichtbaar";
                    }
                } else {
                    for (i = 0; i < photoviewer.images.length; i = i + 1) {
                        imgId = photoviewer.images[i].imgId;
                        document.getElementById(imgId).className = "onzichtbaar";
                    }
                    document.getElementById(auteurTekst).className = "tekst";
                    document.getElementById(datumTekst).className = "tekst";
                    lastZoomed.className = "groteFoto";
                }
            }
            event.preventDefault();
        },

        raster: function () { //Als je een bepaald aantal plaatjes verwijderd past hij de hoeveelheid padding rechts en links aan. Hij berekend de lengte van de rij
            if (!document.getElementsByClassName("groteFoto")[0]) {
                var arrayLengte  = photoviewer.images.length - 1, breedte, lengte;
                breedte = window.innerWidth;
                lengte = Math.ceil(Math.sqrt(arrayLengte));
                document.body.style.paddingLeft = ((breedte - 240 * lengte) / 2) + "px";
                document.body.style.paddingRight = ((breedte - 240 * lengte) / 2) + "px";
                document.body.style.minWidth = (240 * lengte) + "px";
            } else {
                document.body.removeAttribute("style");
            }
        }
    };
    photoviewer.raster();
    window.addEventListener("resize", photoviewer.raster, false);
    photoviewer.Showimages();
    photoviewer.imgDetails();
    window.photoviewer = photoviewer;
}());
