
var enlarged = false;
var selectie = "";
var lastImage = 1;
var count = 1;

var Image = new Array(); 
Image[0] = "images/0004681.jpg";
Image[1] = "images/0004713.jpg";
Image[2] = "images/0004720.jpg";
Image[3] = "images/0004731.jpg";
Image[4] = "images/0004750.jpg";
Image[5] = "images/0004755.jpg";
Image[6] = "images/0004801.jpg";
Image[7] = "images/0004802.jpg";
Image[8] = "images/0004827.jpg";
Image[9] = "images/0004853.jpg";
Image[10] = "images/0004858.jpg";
Image[11] = "images/0004860.jpg";
Image[12] = "images/0004870.jpg";
Image[13] = "images/0004874.jpg";
Image[14] = "images/0004888.jpg";
Image[15] = "images/0004902.jpg";
Image[16] = "images/0004931.jpg";
Image[17] = "images/0004969.jpg";
Image[18] = "images/0004971.jpg";
Image[19] = "images/0006598.jpg";
Image[20] = "images/0006630.jpg";
Image[21] = "images/0006638.jpg";
Image[22] = "images/0006680.jpg";
Image[23] = "images/0006743.jpg";
Image[24] = "images/0006787.jpg";

function displayAllImages() {
	var i, maakPlaatje;
	for (var i=0;i<Image.length;i++) {
		"use strict";
		var random = Math.random()*10-5;
		var lengte = Math.ceil(Math.sqrt(Image.length));
		var breedte = window.innerWidth;
		var marginT = 50;
		var marginX = marginT +"px";
		var width = (breedte - ((lengte + 1) * (marginT * 2)))/lengte + "px";
		document.getElementById("reloadFotos").style.zIndex="-10";
		document.getElementById("fotos").style.marginLeft = marginT +"px";
		document.getElementById("fotos").innerHTML += "<img src='" + Image[i] + "'class='flex-item' onclick='enlarge("+i+")' id='foto"+i+"' oncontextmenu='deleteImage("+i+")' style='-webkit-transform:rotate("+random+"deg); margin: 25px "+ marginX +"; width:"+ width +";' ;/>"
	}
	if(Image.length==0){
		document.getElementById("reloadFotos").style.zIndex="10";
	}
}

function enlarge(welkeFoto){
	selectie = welkeFoto;
	enlarged = true;
	document.getElementById("blackScreen").style.zIndex="10";
	document.getElementById("enlarge").innerHTML += "<img src='" + Image[selectie] + "'class='' id='"+selectie+"';/>"
	document.getElementById("enlarge").innerHTML += "<p>test</p>"
	document.getElementById("enlarge").style.zIndex="10";
	lastImage = selectie;
}

function refocus(){
	enlarged = false;
	document.getElementById("blackScreen").style.zIndex="-10";
	document.getElementById("enlarge").innerHTML = "<img src='';/>"
	document.getElementById("enlarge").style.zIndex="-10";
}

document.onkeydown = checkKey;

function checkKey(e) {
    e = e || window.event;
	if(enlarged){
		if (e.keyCode == '37') {		
			if(selectie != 0 && selectie <= Image.length){
				selectie--;
				document.getElementById("enlarge").innerHTML = "<img src='" + Image[selectie] + "'class='' id='"+selectie+"';/>"
				lastImage = selectie;
			}else if(selectie == 0){
				var tempLastImage;
				tempLastImage = Image.length - 1;
				selectie = tempLastImage;
				document.getElementById("enlarge").innerHTML = "<img src='" + Image[selectie] + "'class='' id='"+selectie+"';/>"
				lastImage = selectie;
			}
		}
		else if (e.keyCode == '39') {
			var absArrLength = Image.length - 1;
			if(selectie != absArrLength){
				selectie++;
				document.getElementById("enlarge").innerHTML = "<img src='" + Image[selectie] + "'class='' id='"+selectie+"';/>"
				lastImage = selectie;
			}else if(selectie == absArrLength){
				var firstElement = Image.length - Image.length;
				selectie = firstElement;
				document.getElementById("enlarge").innerHTML = "<img src='" + Image[selectie] + "'class='' id='"+selectie+"';/>"
				lastImage = selectie;
			}
		}
	}
	if(e.keyCode == '32'){
		e.preventDefault();
		if(!enlarged){
			enlarge(lastImage);
		}
	}
	if(e.keyCode == '27'){
		refocus();
	}
}

function deleteImage(e){
	e = e;
	Image.splice(e, 1);
	document.getElementById("fotos").innerHTML = "";
	displayAllImages();
}

document.oncontextmenu = function() {
    return false;
}










