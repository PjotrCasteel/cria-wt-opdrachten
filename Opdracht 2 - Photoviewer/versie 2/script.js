/**
 * User: Pjotr Casteel
 * Date: 16-9-14
 * Time: 14:35
 *
 * jslint browser: false
 * jslint devel: true
 * jslint node: true
 */
/*global window, document, location*/
(function () {
    "use strict";
    var zoomed = false, i, currentPic, rasterSize = 18,
        photoviewer = {
            images : [
                {"file": "image1.jpg", "author": "auteur", "date": "09-09-1993", "alt": "Blocks"},
                {"file": "image2.jpg", "author": "auteur", "date": "12-12-2012", "alt": "Defqon 2012"},
                {"file": "image3.jpg", "author": "auteur", "date": "05-02-1945", "alt": "prehistorie"},
                {"file": "image4.jpg", "author": "auteur", "date": "09-09-2013", "alt": "New york"},
                {"file": "image5.jpg", "author": "auteur", "date": "07-09-1994", "alt": "Music"},
                {"file": "image6.jpg", "author": "auteur", "date": "07-07-2011", "alt": "Busy"},
                {"file": "image7.jpg", "author": "auteur", "date": "09-09-2013", "alt": "Abstract"},
                {"file": "image8.jpg", "author": "auteur", "date": "09-09-2013", "alt": "Ok"},
                {"file": "image9.jpg", "author": "auteur", "date": "09-09-2013", "alt": "Ape"},
                {"file": "image10.jpg", "author": "Wakkerdier", "date": "09-09-2013", "alt": "Eggs"},
                {"file": "image11.jpg", "author": "auteur", "date": "09-09-2013", "alt": "misty"},
                {"file": "image12.jpg", "author": "auteur", "date": "09-09-2013", "alt": "Rainy day"},
                {"file": "image13.jpg", "author": "auteur", "date": "09-09-2013", "alt": "yolo"},
                {"file": "image14.jpg", "author": "IDC studio", "date": "09-09-2013", "alt": "geen titel"},
                {"file": "image15.jpg", "author": "auteur", "date": "09-09-2013", "alt": "games"},
                {"file": "image16.jpg", "author": "Mark", "date": "09-09-2013", "alt": "Hard gaan"},
                {"file": "image17.jpg", "author": "Benjamin", "date": "09-09-2013", "alt": "i believe i can.."},
                {"file": "image18.jpg", "author": "Joost", "date": "09-09-2013", "alt": "Downhill"},
                {"file": "image19.jpg", "author": "Black", "date": "09-09-2013", "alt": "Geel is alles"},
                {"file": "image20.jpg", "author": "auteur", "date": "09-09-2013", "alt": "OK"},
                {"file": "image21.jpg", "author": "auteur", "date": "09-09-2013", "alt": "Cats:)"},
                {"file": "image22.jpg", "author": "auteur", "date": "09-09-2013", "alt": "police"},
                {"file": "image23.jpg", "author": "Karel", "date": "09-09-2013", "alt": "Jack"},
                {"file": "image24.jpg", "author": "auteur", "date": "09-09-2013", "alt": "painting"},
                {"file": "image25.jpg", "author": "auteur", "date": "09-09-2013", "alt": "half vol of half leeg?"}
            ],
            gallery : document.createElement("div"),
            /* DESCRIPTION: Function Init
               ---------------------------------------------------------------------------------------
               Deze functie word aangeroepen wanneer het scherm voor het eerst opstart,
               deze functie zal ook maar 1 keer aangeroepen worden.

               De init functie creërt alle elementen in de html (en de bijbehorende attributen):
                   1 gallery div,
                   25 images
                   25 teksen (voor bij de images)
                   25 imgContainers (om de image+tekst in te stoppen)
               ---------------------------------------------------------------------------------------
            */
            init : function () {
                var img, imgText, imgContainer;
                photoviewer.gallery.setAttribute('id', 'gallery');
                photoviewer.gallery.setAttribute('onclick', 'photoviewer.startProgram()');
                photoviewer.gallery.style.width = "60%";
                photoviewer.gallery.style.height = "90%";
                photoviewer.gallery.style.backgroundImage = "url(afbeeldingen/click.png)";
                for (i = 0; i < photoviewer.images.length; i += 1) {
                    img = document.createElement("img");
                    img.src = 'afbeeldingen/' + photoviewer.images[i].file;
                    img.alt = photoviewer.images[i].alt;
                    img.setAttribute('id', 'img' + i);
                    img.setAttribute('style', 'display: none');
                    img.oncontextmenu = photoviewer.stopContextMenu();
                    img.onmousedown = photoviewer.onRightMouseDown();
                    //maakt een image

                    imgText = document.createElement("span");
                    imgText.innerHTML = "'" + photoviewer.images[i].alt + "' door " + photoviewer.images[i].author;
                    imgText.setAttribute('id', 'imgText' + i);
                    imgText.setAttribute('style', 'display: none;');
                    //maakt tekst voor bij het plaatje (span tekst)

                    imgContainer = document.createElement("div");
                    imgContainer.setAttribute('id', 'imgContainer' + i);
                    imgContainer.setAttribute('class', 'imgContainer');
                    imgContainer.setAttribute('style', 'width: 18%; height: 18%');
                    //maakt een div om het plaatje heen

                    imgContainer.appendChild(img);                      //stopt de img in de DIV ImgContainer
                    imgContainer.appendChild(imgText);                  //stopt de img tekst in de DIV ImgContainer
                    photoviewer.gallery.appendChild(imgContainer);      //stopt de DIV ImgContainer in de DIV gallery
                }
                document.body.style.backgroundImage = "url(afbeeldingen/background.jpg)";
                document.body.appendChild(photoviewer.gallery);     //stopt de gallery DIV in de body
            },
            /* DESCRIPTION: Function onRightMouseDown
             ---------------------------------------------------------------------------------------
             Deze functie word aangeroepen wanneer er op de rechtermuisknop geklikt wordt (en op een plaatje)
             De rechter muisknop verwijderd het plaatje als erop geklikt word.

             1. Het plaatje gaat weg d.m.v. removeChild.
             2. Het plaatje wordt uit de array (images) gehaald.
             3. Alle id's die zijn meegegeven in de function init worden geupdate (overnieuw gerangschikt van 1 tot <aantal>)
             4. Dan controleert de functie hoeveel images er nog over zijn en past daarbij de grootte aan d.m.v. een raster
             ---------------------------------------------------------------------------------------
            */
            onRightMouseDown : function () {
                return function (event) {
                    if (event.which === 3 && !zoomed) { //right mouse click
                        this.parentNode.parentNode.removeChild(this.parentNode); //verwijderd de imgContainer en inhoud
                        photoviewer.images.splice(this.id, 1);                   //haalt het plaatje uit de array
                        for (i = 0; i < photoviewer.images.length; i += 1) {
                            //deze for loop rangschikt alle id's weer opnieuw
                            document.getElementsByClassName('imgContainer')[i].id = 'imgContainer' + i;
                            document.getElementsByTagName('img')[i].id = 'img' + i;
                            document.getElementsByTagName('span')[i].id = 'imgText' + i;
                        }
                        if (photoviewer.images.length < 1) {
                            location.reload();
                        } else if (photoviewer.images.length < 2) {
                            rasterSize = 100;
                        } else if (photoviewer.images.length < 5) {
                            rasterSize = 48;
                        } else if (photoviewer.images.length < 10) {
                            rasterSize = 31;
                        } else if (photoviewer.images.length < 17) {
                            rasterSize = 23;
                        } else if (photoviewer.images.length < 26) {
                            rasterSize = 18;
                        }
                        for (i = 0; i < photoviewer.images.length; i += 1) {
                            document.getElementsByClassName('imgContainer')[i].setAttribute('style', 'width: ' + rasterSize + '%; height: ' + rasterSize + '%');
                        }
                    } else {
                        photoviewer.zoom(this.id);
                    }
                };
            },
            /* DESCRIPTION: Function stopContextMenu
             ---------------------------------------------------------------------------------------
             De contextmenu staat voor het standaard menu wat verschijnt als er op de rechtermuisknop geklikt wordt.
             Deze functie is erg klein en het enige wat hij doet is; Wanneer er op de rechtermuisknop geklikt wordt,
             wordt het menu tegengehouden zodat het niet te voorschijn komt d.m.v. preventDefault();

             Als er buiten de plaatjes wordt geklikt dan zal het standaard menu nog wel te voorschijn komen.
             ---------------------------------------------------------------------------------------
            */
            stopContextMenu : function () { //deze functie stopt het standaard rechtermuisknop menu
                return function (event) {
                    event.preventDefault();
                };
            },
            /* DESCRIPTION: Function startProgram
             ---------------------------------------------------------------------------------------
             De functie startProgram wordt aangeroepen wanneer er voor het eerst geklikt wordt op de applicatie.

             1. Aan het begin krijgen alle plaatjes een display none mee zodat ze niet worden weergegeven
             2. zodra er op de gallery is geklikt (met een 'Click Me!' plaatje erbij) dan wordt deze functie aangeroepen.
             3. De functie verwijderd alle display's van de images en zet verwijderd het 'Click me!' plaatje.
             4. Tot slot zorgt de globale variabele programStarted ervoor dat deze functie niet vaker kan worden aangeroepen.
             ---------------------------------------------------------------------------------------
             */
            startProgram : function () {
                var programStarted = false;
                if (!programStarted) {
                    for (i = 0; i < photoviewer.images.length; i += 1) {
                        document.getElementById('img' + i).removeAttribute('style');
                    }
                    photoviewer.gallery.style.backgroundImage = "none";
                    programStarted = true;
                }
            },
            /* DESCRIPTION: Function zoomIn
             ---------------------------------------------------------------------------------------
             De functie om in te zoomen op een plaatje.
             deze functie wordt aangeroepen vanuit de zoom functie (hieronder) en de keyboard toetsen.

             1. er wordt een getal meegegeven (pictureID), dit is het plaatje waarop moet worden ingezoomd.
             2. in de for loop verwijderd hij alle andere plaatjes d.m.v. display none.
             3. in de for loop haalt hij de display weg bij de tekst zodat de tekst zichtbaar komt.
             4. na de for loop zoomt hij in op het plaatje d.m.v. de parent 100% te maken.
             het plaatje is al 100% dus alleen de div eromheen hoeft vergroot te worden (100%).
             ---------------------------------------------------------------------------------------
             */
            zoomIn : function (pictureID) {
                // zoomt in op het huidige geselecteerde plaatje.
                currentPic = pictureID;
                var zoomThisPic = document.getElementById('img' + pictureID);
                for (i = 0; i < photoviewer.images.length; i += 1) {
                    document.getElementById('img' + i).parentNode.setAttribute('style', 'display: none;');
                    document.getElementById('imgText' + i).removeAttribute('style');
                    //verwijderd alle andere ImgContainers
                }
                zoomThisPic.parentNode.removeAttribute('style');
                zoomThisPic.parentNode.style.width = "100%";
                zoomThisPic.parentNode.style.height = "100%";
                zoomed = true;
            },
            /* DESCRIPTION: Function zoomOut
             ---------------------------------------------------------------------------------------
             Deze functie maakt de applicatie weer normaal en wordt aangeroepen wanneer er is ingezoomd.
             hij maakt alle parents van de plaatjes weer normaal d.m.v.:

             de display:none weg te halen;
             de width/height weer normaal;
             en de tekst verbergt hij weer.
             ---------------------------------------------------------------------------------------
             */
            zoomOut : function () {
                for (i = 0; i < photoviewer.images.length; i += 1) {
                    document.getElementById('img' + i).parentNode.removeAttribute('style');            //maakt de parent van de plaatjes normaal
                    document.getElementById('img' + i).parentNode.style.width = rasterSize + "%";       //
                    document.getElementById('img' + i).parentNode.style.height = rasterSize + "%";
                    document.getElementById('imgText' + i).setAttribute('style', 'display: none;');     //verwijderd alle tekst
                }
                zoomed = false;
            },
            /* DESCRIPTION: Function zoom
             ---------------------------------------------------------------------------------------
             Deze functie wordt alleen aangeroepen vanuit de image.onclick attribute (DOM LEVEL 2)

             er wordt een id meegegeven van het plaatje, bijvoorbeeld het id van plaatje 15 in de array is dus 'img15'
             vervolgens wordt het woordje 'img' verwijderd door substring zodat er van 'img15' > '15' wordt gemaakt
             dit is nog een String dus wordt met behulp van parseInt een integer.
             Via deze integer (nummer) kan worden ingezoomd op het bepaalde plaatje (15)

             het is van groot belang dat de currentPic in de gehele photoviewer beschikbaar is omdat het bijna overal wordt gebruikt.
             ---------------------------------------------------------------------------------------
             */
            zoom : function (id) {
                currentPic = (id.substring(3, 5));      // maakt van IMG15 > 15
                currentPic = parseInt(currentPic, 10);  // maakt van string '15' > int 15
                if (!zoomed) {
                    photoviewer.zoomIn(currentPic);
                } else if (zoomed) {
                    photoviewer.zoomOut();
                }
            },
            /* DESCRIPTION: Function checkKey
             ---------------------------------------------------------------------------------------
             Deze functie wordt aangeroepen wanneer er een event plaatsvindt.
             onderaan de photoviewer staat 'document.onkeydown = photoviewer.checkKey;'
             hij kijkt naar het window.event (het toetsenbord in dit geval), en met dat event gaat de functie verder.
             vervolgens wordt er gecontroleerd op welk knopje er wordt geklikt d.m.v. event.keyCode
             tot slot wordt er met dat knopje een actie uitgevoerd.
             ---------------------------------------------------------------------------------------
             */
            checkKey : function (e) {             //pijltjesnavigatie
                e = e || window.event;
                if (e.keyCode === 37 && zoomed) { // pijltje links
                    if (currentPic === 0) {
                        photoviewer.zoomIn(photoviewer.images.length - 1);
                    } else {
                        photoviewer.zoomIn(currentPic - 1);
                    }
                }
                if (e.keyCode === 39 && zoomed) { // pijltje rechts
                    if (currentPic === photoviewer.images.length - 1) {
                        photoviewer.zoomIn(0);
                    } else {
                        photoviewer.zoomIn(currentPic + 1);
                    }
                }
                if (e.keyCode === 27 && zoomed) {  //esc knop
                    photoviewer.zoomOut();
                }
                if (e.keyCode === 32) {  //spacebar
                    if (zoomed) {
                        photoviewer.zoomOut();
                    } else {
                        if (currentPic.value) { //als currentPic nog geen value heeft.. doe dan niets
                            photoviewer.zoomOut();
                        } else {
                            photoviewer.zoomIn(currentPic);
                        }
                    }
                }
            }//sluit checkKey()
        }; //sluit photoviewer
    document.onkeydown = photoviewer.checkKey;
    window.photoviewer = photoviewer;
    photoviewer.init();
}());