/**
 * Created by Pjotr Casteel on 17-9-14.
 */
// my test file
describe("Testing for side effects", function () {
    "use strict";
    it("Test for object, imagAr", function () {
        var retVal = photoviewer.imageAr[0].filename;
        expect(retVal).toEqual("abc123.jpg");
    });

    it("Test for arrayCounter", function () {
        var retVal = photoviewer.arrayCounter();
        expect(retVal).toEqual(2);
    });
});