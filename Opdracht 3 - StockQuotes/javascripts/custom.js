/**
 * Created by Pjotr Casteel on 17-9-14.
 */
/*JSLINT browser: true*/
/*JSLINT plusplus: true*/
/*JSLINT document: true*/

var obj, window, console, document, XMLHttpRequest, XDomainRequest, stockquotes, fallBackData;
(function () {
    "use strict";
    var module = {
        init: function () {
            stockquotes.createTables();
            stockquotes.getURL();
            window.setInterval(stockquotes.getURL, 3600);
        },

        createCORSRequest: function (method, url) {
            var xhr;
            xhr = new XMLHttpRequest();
            if (xhr.withCredentials !== undefined) {
                // XHR for Chrome/Firefox/Opera/Safari
                xhr.open(method, url, true);
            } else if (typeof XDomainRequest) {
                // XDomainRequest for IE
                xhr = new XDomainRequest();
                xhr.open(method, url);
            } else {
                // CORS not supported
                xhr = null;
            }
            return xhr;
        },

        getURL: function () {
            var req, link, str;
            link = "http://cria.tezzt.nl/examples/stockquotes/index.php";
            req = stockquotes.createCORSRequest("get", link);
            req.addEventListener("load", function (data) {
                str = data.target.responseText;
                obj = JSON.parse(str);
                stockquotes.updateTablesContent(obj);
            });
            req.send();
            req.onreadystatechange = function () {
                if (req.readyState === 4) {
                    if (req.status === 200) {
                        console.log("online data");
                    } else {
                        console.log("fake offline data");
                        stockquotes.fakeData(fallBackData);
                    }
                }
            };
        },

        fakeData: function (fbData) {
            obj = stockquotes.randomizeFallBackData(fbData);
            stockquotes.updateTablesContent(obj);
        },
        randomizeFallBackData: function (fbData) {
            var row, data, change, percentage, getCurrentDate;
            for (row = 0; row < fallBackData.query.results.row.length; row += 1) {
                data = fbData.query.results.row[row];
                change = stockquotes.intShorterner(stockquotes.randomizer(), 3);
                percentage = (1 + Number(change));
                getCurrentDate = new Date();
                //col0
                //col1
                data.col1 = stockquotes.intShorterner(data.col1 * percentage, 2);
                //col2
                data.col2 = getCurrentDate.getDay() + "/" + getCurrentDate.getMonth() + "/" + getCurrentDate.getFullYear();
                //col3
                data.col3 = getCurrentDate.getHours() + ":" + getCurrentDate.getMinutes();
                //col4
                if (change > 0) {
                    data.col4 = "+" + change;
                } else {
                    data.col4 = change;
                }
                //col5
                if (data.col5 === "N/A") {
                    data.col5 = data.col1;
                }
                //col6
                if (data.col1 > data.col6 || data.col6 === "N/A") {
                    data.col6 = data.col1;
                }
                //col7
                if (data.col1 < data.col7 || data.col7 === "N/A") {
                    data.col7 = data.col1;
                }
                //col8
            }
            return fbData;
        },

        randomizer: function () {
            var random = Math.random();
            if (Math.random() >= 0.5) {
                random = "+" + random;
            } else {
                random = "-" + random;
            }
            return random / 10;
        },

        intShorterner: function (value, decimals) {
            var i, dec;
            dec = 1;
            for (i = 0; i < decimals; i += 1) {
                dec = dec + "0";
            }
            value = (Math.round(value * dec)) / dec;
            return value;
        },

        createTables: function () {
            stockquotes.createMainTable();
            stockquotes.createWinnerTable();
            stockquotes.createDisclamerTab();
            stockquotes.createLoserTable();
        },

        createMainTable: function () {
            var bodyNode, tableNode;
            bodyNode = document.getElementsByTagName("body")[0];
            tableNode = document.createElement("table");
            tableNode.id = "mainTable";
            bodyNode.appendChild(tableNode);
        },

        createWinnerTable: function () {
            var bodyNode, tableNode;
            bodyNode = document.getElementsByTagName("body")[0];
            tableNode = document.createElement("table");
            tableNode.id = "winnerTable";
            bodyNode.appendChild(tableNode);
        },
        createDisclamerTab: function () {
            var bodyNode, divNode;
            bodyNode = document.getElementsByTagName("body")[0];
            divNode = document.createElement("div");
            divNode.id = "disclamerTab";
            bodyNode.appendChild(divNode);
            stockquotes.fillDisclamer(0);
            stockquotes.fillDisclamer(1);
            document.getElementById("disclamerTab").style.width = window.innerWidth + "px";
            document.getElementById("disclamerTab").onclick = function () {
                document.getElementById("disclamerTab").style.visibility = "hidden";
            };
        },

        createLoserTable: function () {
            var bodyNode, tableNode;
            bodyNode = document.getElementsByTagName("body")[0];
            tableNode = document.createElement("table");
            tableNode.id = "loserTable";
            bodyNode.appendChild(tableNode);

        },


        createTableHeader: function (table) {
            var tableNode, tableRowNode, col;
            tableNode = document.getElementsByTagName("table")[table];
            tableRowNode = document.createElement("tr");
            tableRowNode.className = "tableheader";
            tableNode.appendChild(tableRowNode);
            col = document.createElement("th");
            col.textContent = "Symbol";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Last trade price";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Last trade date";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Last trade time";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Change";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Open";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Day's high";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Day's low";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Volume";
            tableRowNode.appendChild(col);

        },

        createTableContent: function (obj, table) {
            var dataArray, rowCounter;
            stockquotes.createTableHeader(table);
            dataArray = obj.query.results.row;
            for (rowCounter = 0; rowCounter < dataArray.length; rowCounter += 1) {
                if (table === 0) {
                    stockquotes.createRow(dataArray[rowCounter], table);
                } else if (table === 1) {
                    if (dataArray[rowCounter].col4 >= 0) {
                        stockquotes.createRow(dataArray[rowCounter], table);
                    }
                } else if (table === 2) {
                    if (dataArray[rowCounter].col4 < 0) {
                        stockquotes.createRow(dataArray[rowCounter], table);
                    }
                }
            }
        },

        createRow: function (data, table) {
            var tableNode, tableRowNode, colNr, colString, colNode;
            tableNode = document.getElementsByTagName("table")[table];
            tableRowNode = document.createElement("tr");
            if (data.col4 < 0) {
                tableRowNode.className = "loser";
            } else {
                tableRowNode.className = "winner";
            }
            tableNode.appendChild(tableRowNode);
            for (colNr = 0; colNr <= 8; colNr += 1) {
                colString = "col" + colNr;
                colNode = document.createElement("td");
                colNode.textContent = data[colString];
                tableRowNode.appendChild(colNode);
            }
        },
        fillDisclamer: function (a) {
            var bodyNode, pNode, number;
            number = a;
            bodyNode = document.getElementById("disclamerTab");
            pNode = document.createElement("p");
            pNode.id = "disclamerTabtext";
            bodyNode.appendChild(pNode);
            if (number === 0) {
                document.getElementById("disclamerTabtext").innerHTML += "Let op! De data in de onderstaande tabellen bevatten fictieve data, de applicatie is ook uitsluiten bedoelt voor onderwijs doeleinden." + "<br>";
            } else {
                document.getElementById("disclamerTabtext").innerHTML += "<span style='font-size:15px;'>" + "Click om te sluiten" + "</span>";
            }
        },

        updateTablesContent: function (obj) {
            var table;
            for (table = 0; table < document.getElementsByTagName("table").length; table += 1) {
                stockquotes.removeTableContent(table);
                stockquotes.createTableContent(obj, table);
            }
        },

        removeTableContent: function (table) {
            var tableNode, rowNode;
            tableNode = document.getElementsByTagName("table")[table];
            rowNode = tableNode.getElementsByTagName("tr");
            while (tableNode.getElementsByTagName("tr").length > 0) {
                tableNode.removeChild(rowNode[0]);
            }
        },

        deleteTables: function () {
            var tableArray, tableNode;
            tableArray = document.getElementsByTagName("table");
            while (tableArray.length > 0) {
                tableNode = document.getElementsByTagName("table")[0];
                tableNode.remove(tableNode);
            }
        }
    };
    window.stockquotes = module;
}());
