Stockquotes
======================
Pjotr Casteel

Design
======================

**init**
----------------------
o create a maintable with all the data in it

o create a winner table with al the current rising company's

o create a loser table with al the current losing company's

o run getURL(), from there getting data from any source and putting t into tables

```sh
init: function () {
            stockquotes.createTables();
            stockquotes.getURL();
            window.setInterval(stockquotes.getURL, 60000);
        }
```

**createCORSRequest**
----------------------
o requests the data from an online source

```sh
createCORSRequest: function (method, url) {
            var xhr;
            xhr = new XMLHttpRequest();
            if (xhr.withCredentials !== undefined) {
                // XHR for Chrome/Firefox/Opera/Safari
                xhr.open(method, url, true);
            } else if (typeof XDomainRequest) {
                // XDomainRequest for IE
                xhr = new XDomainRequest();
                xhr.open(method, url);
            } else {
                // CORS not supported
                xhr = null;
            }
            return xhr;
        }
```

**getURL**
----------------------
o run the CORSRequest to get the online data

o put the response into a JSON

o put the data into the tables

o at the end checks if the online source was found

o when there was no connection run fakeData to load the fallback data

```sh
getURL: function () {
            var req, link, str;
            link = "http://cria.tezzt.nl/examples/stockquotes/index.php";
            req = stockquotes.createCORSRequest("get", link);
            req.addEventListener("load", function (data) {
                str = data.target.responseText;
                obj = JSON.parse(str);
                stockquotes.updateTablesContent(obj);
            });
            req.send();
            req.onreadystatechange = function () {
                if (req.readyState === 4) {
                    if (req.status === 200) {
                        console.log("online data");
                    } else {
                        console.log("fake offline data");
                        stockquotes.fakeData(fallBackData);
                    }
                }
            };
        }
```

**fakeData**
----------------------
o randomize the data of the fallback data

o update the data with the random fallback data

```sh
fakeData: function (fbData) {
            obj = stockquotes.randomizeFallBackData(fbData);
            stockquotes.updateTablesContent(obj);
        }
```

**randomizeFallBackData**
----------------------
o randomize the values, based on the rate like the last bid value, afterwards updating the day low/high

o set the time and date to current

```sh
randomizeFallBackData: function (fbData) {
            var row, data, change, percentage, getCurrentDate;
            for (row = 0; row < fallBackData.query.results.row.length; row += 1) {
                data = fbData.query.results.row[row];
                change = stockquotes.intShorterner(stockquotes.randomizer(), 3);
                percentage = (1 + Number(change));
                getCurrentDate = new Date();
                //col0
                //col1
                data.col1 = stockquotes.intShorterner(data.col1 * percentage, 2);
                //col2
                data.col2 = getCurrentDate.getDay() + "/" + getCurrentDate.getMonth() + "/" + getCurrentDate.getFullYear();
                //col3
                data.col3 = getCurrentDate.getHours() + ":" + getCurrentDate.getMinutes();
                //col4
                if (change > 0) {
                    data.col4 = "+" + change;
                } else {
                    data.col4 = change;
                }
                //col5
                if (data.col5 === "N/A") {
                    data.col5 = data.col1;
                }
                //col6
                if (data.col1 > data.col6 || data.col6 === "N/A") {
                    data.col6 = data.col1;
                }
                //col7
                if (data.col1 < data.col7 || data.col7 === "N/A") {
                    data.col7 = data.col1;
                }
                //col8
            }
            return fbData;
        }
```

**randomizer**
----------------------
o generates a random including a + or - in the beginning

```sh
randomizer: function () {
            var random = Math.random();
            if (Math.random() >= 0.5) {
                random = "+" + random;
            } else {
                random = "-" + random;
            }
            return random / 10;
        }
```

**intShorterner**
----------------------
o shorten the given value with the given variable decimals

```sh
intShorterner: function (value, decimals) {
            var i, dec;
            dec = 1;
            for (i = 0; i < decimals; i += 1) {
                dec = dec + "0";
            }
            value = (Math.round(value * dec)) / dec;
            return value;
        }
```

**createTables**
----------------------
o create the main, winner and loser table with it's own function

```sh
createTables: function () {
            stockquotes.createMainTable();
            stockquotes.createWinnerTable();
            stockquotes.createLoserTable();
        }
```
```sh
createMainTable: function () {
            var bodyNode, tableNode;
            bodyNode = document.getElementsByTagName("body")[0];
            tableNode = document.createElement("table");
            tableNode.id = "mainTable";
            bodyNode.appendChild(tableNode);
        }
```
```sh
createWinnerTable: function () {
            var bodyNode, tableNode;
            bodyNode = document.getElementsByTagName("body")[0];
            tableNode = document.createElement("table");
            tableNode.id = "winnerTable";
            bodyNode.appendChild(tableNode);
        }
```
```sh
createLoserTable: function () {
            var bodyNode, tableNode;
            bodyNode = document.getElementsByTagName("body")[0];
            tableNode = document.createElement("table");
            tableNode.id = "loserTable";
            bodyNode.appendChild(tableNode);
        }
```

**createTableHeader**
----------------------
o create a header for the given table

```sh
createTableHeader: function (table) {
            var tableNode, tableRowNode, col;
            tableNode = document.getElementsByTagName("table")[table];
            tableRowNode = document.createElement("tr");
            tableRowNode.className = "tableheader";
            tableNode.appendChild(tableRowNode);
            col = document.createElement("th");
            col.textContent = "Symbol";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Last trade price";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Last trade date";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Last trade time";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Change";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Open";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Day's high";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Day's low";
            tableRowNode.appendChild(col);
            col = document.createElement("th");
            col.textContent = "Volume";
            tableRowNode.appendChild(col);
        }
```

**createTableContent**
----------------------
o filling the main table with all data using createRow

o filling the winner table with the data of the winning company's using createRow

o filling the loser table with the data of the losing company's using createRow

```sh
createTableContent: function (obj, table) {
            var dataArray, rowCounter;
            stockquotes.createTableHeader(table);
            dataArray = obj.query.results.row;
            for (rowCounter = 0; rowCounter < dataArray.length; rowCounter += 1) {
                if (table === 0) {
                    stockquotes.createRow(dataArray[rowCounter], table);
                } else if (table === 1) {
                    if (dataArray[rowCounter].col4 >= 0) {
                        stockquotes.createRow(dataArray[rowCounter], table);
                    }
                } else if (table === 2) {
                    if (dataArray[rowCounter].col4 < 0) {
                        stockquotes.createRow(dataArray[rowCounter], table);
                    }
                }
            }
        }
```

**createRow**
----------------------
o fills a row in the given table with the given data for that row

o give the row a classname for coloring red or green depending on the rate

o fill the row with div's containing the data from the data

```sh
createRow: function (data, table) {
            var tableNode, tableRowNode, colNr, colString, colNode;
            tableNode = document.getElementsByTagName("table")[table];
            tableRowNode = document.createElement("tr");
            if (data.col4 < 0) {
                tableRowNode.className = "loser";
            } else {
                tableRowNode.className = "winner";
            }
            tableNode.appendChild(tableRowNode);
            for (colNr = 0; colNr <= 8; colNr += 1) {
                colString = "col" + colNr;
                colNode = document.createElement("td");
                colNode.textContent = data[colString];
                tableRowNode.appendChild(colNode);
            }
        }
```

**updateTablesContent**
----------------------
o remove the content of each table using removeTableContent

o create the tablecontent for each table using createTableContent

```sh
updateTablesContent: function (obj) {
            var table;
            for (table = 0; table < document.getElementsByTagName("table").length; table += 1) {
                stockquotes.removeTableContent(table);
                stockquotes.createTableContent(obj, table);
            }
        }
```

**removeTableContent**
----------------------
o remove each row from the given table

```sh
removeTableContent: function (table) {
            var tableNode, rowNode;
            tableNode = document.getElementsByTagName("table")[table];
            rowNode = tableNode.getElementsByTagName("tr");
            while (tableNode.getElementsByTagName("tr").length > 0) {
                tableNode.removeChild(rowNode[0]);
            }
        }
```

**deleteTables**
----------------------
o delete all the tables on the page

```sh
deleteTables: function () {
            var tableArray, tableNode;
            tableArray = document.getElementsByTagName("table");
            while (tableArray.length > 0) {
                tableNode = document.getElementsByTagName("table")[0];
                tableNode.remove(tableNode);
            }
        }
```