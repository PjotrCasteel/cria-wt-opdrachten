describe("Tabellen", function () {
    it("Test of 3 tabellen gemaakt zijn", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("table").length).toEqual(3);
        stockquotes.deleteTables();
    });
    it("Test of 53 table rows gemaakt zijn", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("tr").length).toEqual(53);
        stockquotes.deleteTables();
    });
    it("Test of 3 table headers gemaakt zijn", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th").length / 9).toEqual(3);
        stockquotes.deleteTables();
    });
    it("Test of table headers 9 kolommen hebben", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th").length / 3).toEqual(9);
        stockquotes.deleteTables();
    });
});

describe("init()", function () {
    it("Test of 3 tabellen gemaakt zijn via de init zoals de site runt", function () {
        stockquotes.init();
        expect(document.getElementsByTagName("table").length).toEqual(3);
        stockquotes.deleteTables();
    });
});

describe("fakeData()", function () {
    it("Test of obj de fallback data bevat", function () {
        stockquotes.fakeData(fallBackData);
        expect(obj).toEqual(fallBackData);
        stockquotes.deleteTables();
    });
    it("Test of random veld overeenkomt met de waarde in de fallback data", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        var colString;
        for (var row = 0; row < fallBackData.query.results.row.length; row += 1) {
            for (var col = 0; col < 9; col += 1) {
                colString = "col" + col;
                expect(document.getElementsByTagName("td")[row * 9 + col].textContent).toEqual('' + fallBackData.query.results.row[row][colString]);
            }
        }
        stockquotes.deleteTables();
    })
});

describe("randomizeFallBackData()", function () {
    it("Test of data voor en na randomizen niet het zelfde is", function () {
        var before = fallBackData;
        var after = Object.create(before);
        stockquotes.randomizeFallBackData(before);
        expect(after === before).toEqual(false);
    });
});

describe("randomizer()", function () {
    it("Test of waarde een int is", function () {
        var randomized = stockquotes.randomizer();
        var int = Number(randomized + 1);
        expect(randomized + 1).toEqual(int);
    });
});

describe("intShorterner()", function () {
    it("Test of int afgekort word", function () {
        var before = Math.random();
        var after = stockquotes.intShorterner(before, 3);
        expect(before !== after).toEqual(true);
    });
});

describe("createTables()", function () {
    it("Test of main table gemaakt is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("table")[0].id).toEqual("mainTable");
        stockquotes.deleteTables();
    });
    it("Test of winner table gemaakt is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("table")[1].id).toEqual("winnerTable");
        stockquotes.deleteTables();
    });
    it("Test of loser table gemaakt is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("table")[2].id).toEqual("loserTable");
        stockquotes.deleteTables();
    });
});

describe("createMainTable()", function () {
    it("Test of main table gemaakt is", function () {
        stockquotes.createMainTable();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("table")[0].id).toEqual("mainTable");
        stockquotes.deleteTables();
    });
});

describe("createWinnerTable()", function () {
    it("Test of winner table gemaakt is", function () {
        stockquotes.createWinnerTable();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("table")[0].id).toEqual("winnerTable");
        stockquotes.deleteTables();
    });
});

describe("createLoserTable()", function () {
    it("Test of loser table gemaakt is", function () {
        stockquotes.createLoserTable();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("table")[0].id).toEqual("loserTable");
        stockquotes.deleteTables();
    });
});

describe("createTableHeader()", function () {
    it("Test of eerste kop 'Symbol' is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th")[0].textContent).toEqual("Symbol");
        stockquotes.deleteTables();
    });
    it("Test of eerste kop 'Last trade price' is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th")[1].textContent).toEqual("Last trade price");
        stockquotes.deleteTables();
    });
    it("Test of eerste kop 'Last trade date' is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th")[2].textContent).toEqual("Last trade date");
        stockquotes.deleteTables();
    });
    it("Test of eerste kop 'Last trade time' is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th")[3].textContent).toEqual("Last trade time");
        stockquotes.deleteTables();
    });
    it("Test of eerste kop 'Change' is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th")[4].textContent).toEqual("Change");
        stockquotes.deleteTables();
    });
    it("Test of eerste kop 'Open' is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th")[5].textContent).toEqual("Open");
        stockquotes.deleteTables();
    });
    it("Test of eerste kop 'Day's high' is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th")[6].textContent).toEqual("Day's high");
        stockquotes.deleteTables();
    });
    it("Test of eerste kop 'Day's low' is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th")[7].textContent).toEqual("Day's low");
        stockquotes.deleteTables();
    });
    it("Test of eerste kop 'Volume' is", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("th")[8].textContent).toEqual("Volume");
        stockquotes.deleteTables();
    });
});

describe("createTableContent()", function () {
    it("Test of main tabel gemaakt is", function () {
        stockquotes.createMainTable();
        stockquotes.createTableContent(fallBackData, 0);
        expect(document.getElementsByTagName("table").length).toEqual(1);
        stockquotes.deleteTables();
    });
    it("Test of alle regels gemaakt zijn", function () {
        stockquotes.createMainTable();
        stockquotes.createTableContent(fallBackData, 0);
        expect(document.getElementsByTagName("tr").length).toEqual(fallBackData.query.results.row.length + 1);
        stockquotes.deleteTables();
    });
});

describe("createRow()", function () {
    it("Test of data in de gemaakte row klopt bij de data uit de fallback data", function () {
        stockquotes.createMainTable();
        stockquotes.createRow(fallBackData.query.results.row[0], 0);
        for (var col = 0; col < 9; col += 1) {
            var colString = "col" + col;
            var rowNode = document.getElementsByTagName("tr")[0];
            expect(rowNode.getElementsByTagName("td")[col].textContent).toEqual('' + fallBackData.query.results.row[0][colString]);
        }
        stockquotes.deleteTables();
    });
});

describe("updateTablesContent()", function () {
    it("Test of tabellen verwijdert zijn en daarna weer zijn aangemaakt", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        stockquotes.updateTablesContent(fallBackData);
        expect(document.getElementsByTagName("table").length).toEqual(3);
        stockquotes.deleteTables();
    });
    it("Test of tablecontent worden aangemaakt als deze niet bestaan", function () {
        stockquotes.createTables();
        stockquotes.updateTablesContent(fallBackData);
        expect(document.getElementsByTagName("table").length).toEqual(3);
        stockquotes.deleteTables();
    });
});

describe("removeTableContent()", function () {
    it("Test of 3 tabellen gemaakt zijn", function () {
        stockquotes.createTables();
        stockquotes.createTableContent(fallBackData, 0);
        stockquotes.removeTableContent(0);
        expect(document.getElementsByTagName("table").length).toEqual(3);
        stockquotes.deleteTables();
    });
    it("Test of de main tabel leeg gemaakt is", function () {
        stockquotes.createMainTable();
        stockquotes.createTableContent(fallBackData, 0);
        stockquotes.removeTableContent(0);
        var tableNode = document.getElementsByTagName("table")[0];
        expect(tableNode.getElementsByTagName("td").length).toEqual(0);
        stockquotes.deleteTables();
    });
});

describe("deleteTables()", function () {
    it("Test of 3 tabellen verwijdert zijn", function () {
        stockquotes.createTables();
        stockquotes.fakeData(fallBackData);
        expect(document.getElementsByTagName("table").length).toEqual(3);
        stockquotes.deleteTables();
        expect(document.getElementsByTagName("table").length).toEqual(0);
    });
});