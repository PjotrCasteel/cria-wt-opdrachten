Welkom op mijn git repository.
====================
Op mijn Cria repository staan alle opdrachten die ik voor het semester Cria moet maken.

Tot op heden zijn dat de volgende onderwerpen:

- [CSS-light (opdracht 1)](https://bitbucket.org/PjotrCasteel/cria-wt-opdrachten/src/c69cdd807bd6bc5b2d6eca1e301329406bd8a180/Opdracht%201%20-%20CSSLight/uitvoering/?at=master).
- [Photoviewer (opdracht 2)](https://bitbucket.org/PjotrCasteel/cria-wt-opdrachten/src/fb127bc44c1316a60880cb843cf1c96416284738/Opdracht%202%20-%20Photoviewer/versie%202/?at=master).
- [Stockquotes (opdracht 3)](https://bitbucket.org/PjotrCasteel/cria-wt-opdrachten/src/6c0193c2fbf157cc6ac32069bc750ac212737e68/Opdracht%203%20-%20StockQuotes/?at=master).
